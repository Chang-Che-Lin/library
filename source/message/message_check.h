#ifndef _MESSAGE_CHECK_H
#define _MESSAGE_CHECK_H

#include "common/custom_types.h"
#include "message_factory.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
    IMessage *bindingCRC(IN const char *name, IN const char *specifyCheck, uint8_t specifyCheckNumber);
    bool processCRC(IN messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers);
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _MESSAGE_CHECK_H