#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "message_factory.h"

messageComponents *makeComponents(IN uint8_t number)
{
    messageComponents *object = malloc(sizeof(messageComponents));
    object->number = number;
    object->binding = malloc(sizeof(IMessage *) * number);

    return object;
}

void freeComponent(OUT messageComponents *components)
{
    for (int i = 0; i < components->number; i++)
    {
        if (components->binding[i]->type == typeString)
        {
            free(((messageString *)components->binding[i])->string);
        }
        free(components->binding[i]);
    }
    free(components->binding);
    free(components);
}

messageValue *makeValue(IN const char *name, IN uint8_t value)
{
    messageValue *object = malloc(sizeof(messageValue));

    object->type = typeValue;
    strcpy(object->name, name);
    object->size = 1;
    object->value = value;

    return object;
}

messageString *makeString(IN const char *name, IN const char *string, IN uint8_t stringNumber)
{
    messageString *object = malloc(sizeof(messageString));
    uint8_t stringLength = strlen(string) + 1;
    object->string = malloc(sizeof(char) * stringLength);

    object->type = typeString;
    strcpy(object->name, name);
    object->size = 1;
    memset(object->string, '\0', stringLength);
    memcpy(object->string, string, stringLength);
    object->stringNumber = stringNumber;

    return object;
}

uint32_t getComponentIndex(IN messageComponents *components, IN const char *name)
{
    uint32_t index = 0xFFFF;
    uint32_t size = strlen(name);

    for (uint32_t i = 0; i < components->number; i++)
    {
        if (!strncasecmp(name, components->binding[i]->name, size))
        {
            index = i;
            break;
        }
    }

    return index;
}

bool setComponentSize(OUT messageComponents *components, IN const char *name, IN uint32_t size)
{
    bool result = false;

    uint32_t index = getComponentIndex(components, name);

    if (0xFFFF != index)
    {
        components->binding[index]->size = size;
        result = true;
    }

    return result;
}

uint32_t getComponentSize(IN messageComponents *components, IN const char *name)
{
    uint32_t size = 0;
    uint32_t index = getComponentIndex(components, name);

    if (0xFFFF != index)
    {
        size = components->binding[index]->size;
    }

    return size;
}

bool setComponentState(OUT messageComponents *components, IN uint32_t index, processState state)
{
    components->binding[index]->state = state;

    return true;
}

processState getComponentState(OUT messageComponents *components, IN uint32_t index)
{
    return components->binding[index]->state;
}

bool executeComponentProcess(OUT messageComponents *components, IN uint32_t index, IN const uint8_t *data, IN uint32_t numbers)
{
    return components->binding[index]->process(components, index, data, numbers);
}