#include <stdio.h>

#include "encrypt/encrypt.h"
#include "message_application.h"
#include "message_cJSON.h"
#include "message_head.h"

void registerComponent(void)
{
    messageComponents *Components = makeComponents(6);
    char buffer[] = {0xe8};

    Components->binding[0] = bindingHead("HEAD", 0xE8);
    Components->binding[1] = bindingCRC("HCRC", "HEAD", 1);
    Components->binding[2] = bindingHead("CMD", 0xE8);
    Components->binding[3] = bindingHead("LENGTH", 0xE8);
    Components->binding[4] = bindingHead("DATA", 0xE8);
    Components->binding[5] = bindingHead("CRC", 0xE8);

    printf("Components binding finish\r\n");
    // for (int i = 0; i < sizeof(component) / sizeof(IMessage); i++)
    Components->binding[0]->process(Components, 0, buffer, 1);

    freeComponent(Components);
}
