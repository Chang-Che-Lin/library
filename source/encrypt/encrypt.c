#include "encrypt.h"

uint8_t crc8(uint8_t *data, uint16_t length)
{
    const uint8_t polynomial = 0x8D;
    uint8_t remainder = 0;
    uint16_t byteIdx;
    uint16_t bitIdx;

    for (byteIdx = 0; byteIdx < length; byteIdx++)
    {
        remainder ^= data[byteIdx];
        for (bitIdx = 0; bitIdx < 8; bitIdx++)
        {
            if ((remainder & 0x80) == 0x80)
            {
                remainder = (uint8_t)((remainder << 1) ^ polynomial);
            }
            else
            {
                remainder = (uint8_t)(remainder << 1);
            }
        }
    }
    return (remainder);
}