#include "message_component.h"

void *message_moduleNew(const char *moduleName) {
  messageModule *module = calloc(1, sizeof(messageModule));

  module->send.callBack = NULL;

  module->moduleName = calloc(strlen(moduleName) + 1, sizeof(uint8_t));
  memcpy(module->moduleName, moduleName, strlen(moduleName));

  module->components.specify = NULL;
  module->components.number = 0;
  module->components.index = 0;

  module->processes.specify = NULL;
  module->processes.number = 0;

  module->send.state = false;

  module->response.component = NULL;
  module->response.resultTrue = NULL;
  module->response.resultFalse = NULL;

  return (void *)module;
}

void message_moduleFree(void *specifyModule) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *componentAddr[module->components.number];
  messageProcess *processAddr[module->processes.number];
  memset(componentAddr, '\0', module->components.number);
  memset(processAddr, '\0', module->processes.number);

  /* free component */
  messageComponent *currentComponent = module->components.specify;

  for (uint32_t i = 0; i < module->components.number; i++)
  {
    componentAddr[i] = currentComponent;
    currentComponent = currentComponent->next;
  }

  for (uint32_t i = module->components.number; i > 0; i--)
  {
    componentAddr[i - 1]->next = NULL;

    if (componentAddr[i - 1]->free != NULL)
      componentAddr[i - 1]->free(componentAddr[i - 1]);

    free(componentAddr[i - 1]->spaceName);
    free(componentAddr[i - 1]->receiveData);
    free(componentAddr[i - 1]->sendData);

    free(componentAddr[i - 1]);
  }

  module->components.specify = NULL;

  /* free process */
  messageProcess *currentProcess = module->processes.specify;

  for (uint32_t i = 0; i < module->processes.number; i++)
  {
    processAddr[i] = currentProcess;
    currentProcess = currentProcess->next;
  }

  for (uint32_t i = module->processes.number; i > 0; i--)
  {
    processAddr[i - 1]->next = NULL;

    free(processAddr[i - 1]->name);
    free(processAddr[i - 1]);
  }

  module->processes.specify = NULL;

  /* free response */
  free(module->response.resultTrue);
  free(module->response.resultFalse);
  module->response.component = NULL;

  /* free module */
  free(module->moduleName);
  free(module);
}

void *message_processGetSpecify(void *specifyModule, const char *processName) {
  messageModule *module = (messageModule *)specifyModule;
  messageProcess *current = module->processes.specify;

  while (current != NULL) {
    if (memcmp(processName, current->name, strlen(current->name)) == 0)
      return (void *)current;
    else
      current = current->next;
  };

  printf("%s have not register\r\n", processName);

  return NULL;
}

void message_processRedirect(void *specifyProcess) {
  messageProcess *process = specifyProcess;

  *(process->map) = process->save;
}

void message_processExecute(void *specifyModule, void *specifyComponent, void *specifyProcess) {
  messageProcess *process = specifyProcess;

  if (process->save != NULL)
    process->save(specifyModule, specifyComponent);
}

bool message_processSpecify(void *specifyModule, void *specifyComponent) {
  messageComponent *component = specifyComponent;
  const char *processName = message_componentGetReceiveData(specifyComponent);
  messageProcess *process = message_processGetSpecify(specifyModule, processName);

  if (process != NULL) {
    if (process->map == &(component->receiveIdleProcess))
      message_processExecute(specifyModule, specifyComponent, process);
    else
      message_processRedirect(process);
    return true;
  }
  else {
    return false;
  }
}

void message_sendSetCallBack(void *specifyModule, void *port, int (*callBack)(void *port, const char *data, int dataLength)) {
  messageModule *module = (messageModule *)specifyModule;
  module->send.callBack = callBack;
  module->send.port = port;
}

void message_sendSetResponse(void *specifyModule, const char *componentName, const char *resultTrue, const char *resultFalse) {
  messageModule *module = (messageModule *)specifyModule;

  module->response.component = message_componentGetSpecify(module, componentName);
  module->response.resultTrue = calloc(strlen(resultTrue) + 1, sizeof(uint8_t));
  memcpy(module->response.resultTrue, resultTrue, strlen(resultTrue));
  module->response.resultFalse = calloc(strlen(resultFalse) + 1, sizeof(uint8_t));
  memcpy(module->response.resultFalse, resultFalse, strlen(resultFalse));
}

bool message_sendGetState(void *specifyModule) {
  messageModule *module = (messageModule *)specifyModule;
  return module->send.state;
}

void message_sendSetState(void *specifyModule, bool state) {
  messageModule *module = (messageModule *)specifyModule;
  module->send.state = state;
}

void message_sendSaveData(void *specifyModule, const char *componentName, const char *data, int dataLength) {
  messageComponent *component = message_componentGetSpecify(specifyModule, componentName);

  if (component->type == typeVariableSpace)
  {
    /* variable space data save */
    free(component->sendData);
    component->sendData = calloc(dataLength + 1, sizeof(uint8_t));
    memcpy(component->sendData, data, dataLength);
    component->sendBytesNum = dataLength;

    /* variable space data length save */
    char specifyStart[strlen(component->spaceName) + 4];
    memset(specifyStart, '\0', sizeof(specifyStart));
    snprintf(specifyStart, sizeof(specifyStart), "%sLen", component->spaceName);
    component = message_componentGetSpecify(specifyModule, specifyStart);
    free(component->sendData);
    component->sendData = message_componentGetNumberConvertBytesBigEndian(dataLength, component->sendBytesNum);
  }
  else if (component->type == typeStaticSpace)
  {
    /* static space data save */
    if (dataLength > component->sendBytesNum)
      memcpy(component->sendData, data, component->sendBytesNum);
    else
      memcpy(component->sendData, data, dataLength);
  }

  message_sendSetState(specifyModule, true);
}

void message_sendSaveResponse(void *specifyModule, bool result) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *component = module->response.component;
  char *responseData = NULL;
  uint32_t responseDataLength = 0;

  if (module->response.component == NULL)
    return;

  if (result == true)
    responseData = module->response.resultTrue;
  else
    responseData = module->response.resultFalse;
  responseDataLength = strlen(responseData);

  if (component->type == typeVariableSpace)
  {
    /* variable space data save */
    free(component->sendData);
    component->sendData = calloc(responseDataLength + 1, sizeof(uint8_t));
    memcpy(component->sendData, responseData, responseDataLength);
    component->sendBytesNum = responseDataLength;

    /* variable space data length save */
    char specifyStart[strlen(component->spaceName) + 4];
    memset(specifyStart, '\0', sizeof(specifyStart));
    snprintf(specifyStart, sizeof(specifyStart), "%sLen", component->spaceName);
    component = message_componentGetSpecify(specifyModule, specifyStart);
    free(component->sendData);
    component->sendData = message_componentGetNumberConvertBytesBigEndian(responseDataLength, component->sendBytesNum);
  }
  else if (component->type == typeStaticSpace)
  {
    /* static space data save */
    if (responseDataLength > component->sendBytesNum)
      memcpy(component->sendData, responseData, component->sendBytesNum);
    else
      memcpy(component->sendData, responseData, responseDataLength);
  }

  message_sendSetState(specifyModule, true);
}

void message_responseSaveSendData(void *specifyModule, void *specifyComponent) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *component = (messageComponent *)specifyComponent;

  if (module->response.component == NULL)
    return;

  if (component->type == typeVariableSpace)
  {
    component->sendBytesNum = component->receiveBytesNum;
    component->sendData = calloc(component->receiveBytesNum + 1, sizeof(uint8_t));
  }
  memcpy(component->sendData, component->receiveData, component->receiveBytesNum);
}

unsigned int message_componentGetIndex(messageModule *module) {
  return module->components.index;
}

void message_componentAddIndex(messageModule *module, uint32_t Num) {
  module->components.index += Num;
}

void message_componentClearIndex(messageModule *module) {
  module->components.index = 0;
}

messageState message_componentGetState(messageComponent *component) {
  return component->state;
}

void message_componentSetState(messageComponent *component, messageState state) {
  component->state = state;
}

void message_componentClearAllStatus(void *specifyModule) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *current = module->components.specify;

  do {
    message_componentSetState(current, stateUndo);
    current = current->next;
  } while (current != NULL);
}

int message_componentGetReceiveDataLength(void *component) {
  return ((messageComponent *)component)->receiveBytesNum;
}

const char *message_componentGetReceiveData(void *component) {
  return (char *)((messageComponent *)component)->receiveData;
}

int message_componentGetSendDataLength(void *component) {
  return ((messageComponent *)component)->sendBytesNum;
}

const char *message_componentGetSendData(void *component) {
  return (char *)((messageComponent *)component)->sendData;
}

unsigned int message_componentGetDataBufferLength(void *specifyComponent, void *startComponent, int (*dataLengthType)(void *component)) {
  messageComponent *component = (messageComponent *)specifyComponent;
  messageComponent *current = (messageComponent *)startComponent;
  unsigned int length = 0;

  while (current != component)
  {
    // printf("spaceName:%s\r\n", current->spaceName);
    // printf("spaceLength:%d\r\n", dataLengthType(current));
    length += dataLengthType(current);
    current = current->next;
  }

  return length;
}

unsigned char *message_componentGetDataBuffer(void *specifyComponent, void *startComponent, unsigned int bufferLength, const char *(*dataBufferType)(void *component), int (*dataLengthType)(void *component)) {
  messageComponent *component = (messageComponent *)specifyComponent;
  messageComponent *current = (messageComponent *)startComponent;
  unsigned char *buffer = NULL;
  unsigned char *pBuffer = NULL;

  buffer = calloc(bufferLength + 1, sizeof(unsigned char));
  pBuffer = buffer;
  while (current != component)
  {
    // printf("spaceName:%s\r\n", current->spaceName);
    // printf("spaceLength:%d\r\n", dataLengthType(current));
    // printf("spaceBuffer: ");
    // for (int i = 0; i < dataLengthType(current); i++)
    // {
    //   printf("%02X ", dataBufferType(current)[i]);
    // }
    // printf("\r\n");

    memcpy(pBuffer, dataBufferType(current), dataLengthType(current));
    pBuffer += dataLengthType(current);
    current = current->next;
  }

  return buffer;
}

unsigned char *message_componentGetNumberConvertBytesBigEndian(int number, int maxBytesNum) {
  unsigned int num = number;
  unsigned char *buffer = NULL;
  unsigned int count = 0;

  if (maxBytesNum == 0) {
    do
    {
      count++;
      num >>= 8;
    } while (num > 0);
    num = number;
  }
  else {
    count = maxBytesNum;
  }

  buffer = calloc(count + 1, sizeof(uint8_t));

  do
  {
    count--;
    buffer[count] = num & 0xFF;
    num >>= 8;
  } while (count > 0);

  return buffer;
}

unsigned int message_componentGetBytesConvertNumberBigEndian(const char *bytes, int bytesNum) {
  unsigned int num = 0;
  unsigned char *buffer = (unsigned char *)bytes;

  for (unsigned int i = 0; i < bytesNum; i++)
  {
    num <<= 8;
    num += buffer[i];
  }

  return num;
}

messageComponent *message_componentGetNext(messageComponent *component) {
  return component->next;
}

messageComponent *message_componentGetFirst(messageModule *module) {
  return module->components.specify;
}

messageComponent *message_componentGetLast(messageModule *module) {
  messageComponent *current = module->components.specify;

  do {
    current = current->next;
  } while (current->next != NULL);

  return current;
}

messageComponent *message_componentGetSpecify(void *specifyModule, const char *componentName) {
  messageModule *module = (messageModule *)specifyModule;
  messageComponent *current = module->components.specify;

  do {
    if (memcmp(componentName, current->spaceName, strlen(current->spaceName)) == 0)
      return current;
    else
      current = current->next;
  } while (current != NULL);

  return NULL;
}

void *message_componentGetSpecifyFront(void *specifyModule, const char *componentName) {
  messageComponent *complexComponent = message_componentGetSpecify(specifyModule, componentName);

  if (complexComponent->type == typeVariableSpace)
  {
    char specifyStart[strlen(complexComponent->spaceName) + 4];
    memset(specifyStart, '\0', sizeof(specifyStart));
    snprintf(specifyStart, sizeof(specifyStart), "%sLen", complexComponent->spaceName);
    complexComponent = message_componentGetSpecify(specifyModule, specifyStart);
  }
  // printf("complexComponent:%s\r\n", complexComponent->spaceName);

  return complexComponent;
}

unsigned int message_componentReceiveSaveData(messageModule *module, messageComponent *component, uint8_t *data, uint32_t bytesNum) {
  unsigned int remainNum = component->receiveBytesNum - message_componentGetIndex(module);
  char *remainDataSpace = (char *)(component->receiveData + message_componentGetIndex(module));
  if (bytesNum > remainNum) {
    memcpy(remainDataSpace, data, remainNum);
    message_componentAddIndex(module, remainNum);
    return remainNum;
  }
  else {
    memcpy(remainDataSpace, data, bytesNum);
    message_componentAddIndex(module, bytesNum);
    return bytesNum;
  }
}

bool message_componentIsDataFull(messageModule *module, messageComponent *component) {
  if (module->components.index < component->receiveBytesNum)
    return false;
  else
    return true;
}

void message_componentReceivePromptlyPreProcess(messageModule *module, messageComponent *component) {
  if (component->receivePromptlyPreProcess != NULL) {
    component->receivePromptlyPreProcess(module, component);
  }
}

bool message_componentReceivePromptlyProcess(messageModule *module, messageComponent *component) {
  if (component->receivePromptlyProcess != NULL) {
    return component->receivePromptlyProcess(module, component);
  }
  else {
    return true;
  }
}

void message_componentReceivePromptlyPostProcess(messageModule *module, messageComponent *component, bool result) {
  if (component->receivePromptlyPostProcess != NULL) {
    component->receivePromptlyPostProcess(module, component, result);
  }
}

void message_componentReceiveIdlePreProcess(messageModule *module, messageComponent *component) {
  if (component->receiveIdlePreProcess != NULL) {
    component->receiveIdlePreProcess(module, component);
  }
}

bool message_componentReceiveIdleProcess(messageModule *module, messageComponent *component) {
  if (component->receiveIdleProcess != NULL) {
    return component->receiveIdleProcess(module, component);
  }
  else {
    return true;
  }
}

void message_componentReceiveIdlePostProcess(messageModule *module, messageComponent *component) {
  if (component->receiveIdlePostProcess != NULL) {
    component->receiveIdlePostProcess(module, component);
  }
}

void message_componentSendIdlePreProcess(messageModule *module, messageComponent *component) {
  if (component->sendIdlePreProcess != NULL) {
    component->sendIdlePreProcess(module, component);
  }
}

void message_componentSendIdlePostProcess(messageModule *module, messageComponent *component) {
  if (component->sendIdlePostProcess != NULL) {
    component->sendIdlePostProcess(module, component);
  }
}

void message_componentSendDataProcess(messageModule *module, messageComponent *component) {
  if (module->send.callBack == NULL)
    return;

  module->send.callBack(module->send.port, (char *)component->sendData, component->sendBytesNum);
}

void message_componentForwardOrReturn(messageModule *module, messageComponent **component, bool result) {
  if (result == true)
  {
    message_componentSetState(*component, stateDone);
    *component = message_componentGetNext(*component);
  }
  else {
    message_componentClearAllStatus(module);
    *component = message_componentGetFirst(module);
  }

  message_componentClearIndex(module);
}

void message_componentReset(void *specifyComponent, unsigned char *specifyResetData, unsigned int specifyResetDataLength) {
  messageComponent *component = (messageComponent *)specifyComponent;

  if (specifyResetData == NULL)
    return;

  memset(specifyResetData, '\0', specifyResetDataLength);
}

void message_componentPush(messageModule *module, messageComponent *component) {
  if (module->components.specify == NULL)
  {
    module->components.specify = component;
  }
  else
  {
    messageComponent *current = module->components.specify;

    while (current->next != NULL)
    {
      current = current->next;
    }

    current->next = component;
  }

  module->components.number++;
}

void message_componentBinding(messageModule *module, messageComponent *component) {
  if (module == NULL) {
    printf("module is null\r\n");
    return;
  }

  if (component == NULL) {
    printf("component is null\r\n");
    return;
  }

  component->next = NULL;

  message_componentPush(module, component);
}

messageComponent *message_assignStaticSpace(messageModule *module, const char *spaceName, unsigned char spaceSize, unsigned int bytesNum) {
  messageComponent *component = calloc(1, spaceSize);

  component->receivePromptlyPreProcess = NULL;
  component->receivePromptlyProcess = NULL;
  component->receivePromptlyPostProcess = NULL;
  component->receiveIdlePreProcess = NULL;
  component->receiveIdleProcess = NULL;
  component->receiveIdlePostProcess = NULL;
  component->sendIdlePreProcess = NULL;
  component->sendIdlePostProcess = NULL;
  component->free = NULL;

  component->spaceName = calloc(strlen(spaceName) + 1, sizeof(unsigned char));
  memcpy(component->spaceName, spaceName, strlen(spaceName));
  component->type = typeStaticSpace;
  component->state = stateUndo;
  component->receiveBytesNum = bytesNum;
  component->receiveData = calloc(bytesNum + 1, sizeof(unsigned char));
  component->sendBytesNum = bytesNum;
  component->sendData = calloc(bytesNum + 1, sizeof(unsigned char));

  message_componentBinding(module, component);
  return component;
}

messageComponent *message_assignVariableSpace(messageModule *module, const char *spaceName, unsigned char spaceSize) {
  messageComponent *component = calloc(1, spaceSize);

  component->receivePromptlyPreProcess = NULL;
  component->receivePromptlyProcess = NULL;
  component->receivePromptlyPostProcess = NULL;
  component->receiveIdlePreProcess = NULL;
  component->receiveIdleProcess = NULL;
  component->receiveIdlePostProcess = NULL;
  component->sendIdlePreProcess = NULL;
  component->sendIdlePostProcess = NULL;
  component->free = NULL;

  component->spaceName = calloc(strlen(spaceName) + 1, sizeof(unsigned char));
  memcpy(component->spaceName, spaceName, strlen(spaceName));
  component->type = typeVariableSpace;
  component->state = stateUndo;
  component->receiveBytesNum = 0;
  component->receiveData = NULL;
  component->sendBytesNum = 0;
  component->sendData = NULL;

  message_componentBinding(module, component);
  return component;
}

void message_processPush(messageModule *specifyModule, messageProcess *specifyProcess) {
  if (specifyModule->processes.specify == NULL)
  {
    specifyModule->processes.specify = specifyProcess;
  }
  else
  {
    messageProcess *current = specifyModule->processes.specify;

    while (current->next != NULL)
    {
      current = current->next;
    }

    current->next = specifyProcess;
  }

  specifyModule->processes.number++;
}

void message_processBinding(messageModule *specifyModule, messageProcess *specifyProcess) {
  if (specifyModule == NULL) {
    printf("module is null\r\n");
    return;
  }

  if (specifyProcess == NULL) {
    printf("process is null\r\n");
    return;
  }

  specifyProcess->next = NULL;

  message_processPush(specifyModule, specifyProcess);
}

messageProcess *message_assignProcess(messageModule *module, const char *processName, const char *componentName, bool (*specifyProcess)(void *specifyModule, void *specifyComponent)) {
  messageProcess *process = calloc(1, sizeof(messageProcess));

  process->name = calloc(strlen(processName) + 1, sizeof(uint8_t));
  memcpy(process->name, processName, strlen(processName));
  messageComponent *component = message_componentGetSpecify(module, componentName);
  process->map = &(component->receiveIdleProcess);
  process->save = specifyProcess;

  message_processBinding(module, process);
  return process;
}

void message_printf(messageModule *module) {
  messageComponent *current = module->components.specify;
  printf("current: %p\r\n", current);

  do {
    // if (current->freeUse != NULL)
    // {
    //   for (uint32_t i = 0; i < current->bytesNum; i++)
    //   {
    //     printf("%x ", current->freeUse[i]);
    //   }
    //   printf("\r\n");
    // }

    current = current->next;
    printf("current: %p\r\n", current);
  } while (current != NULL);
}
