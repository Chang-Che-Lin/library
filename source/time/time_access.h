#ifndef _TIME_ACCESS_H
#define _TIME_ACCESS_H

#include "common/custom_types.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void getCurrentDate(IN OUT char *date, IN uint32_t size);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _TIME_ACCESS_H