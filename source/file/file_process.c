#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

#include "common/custom_types.h"
#include "common/custom_errors.h"
#include "file_access.h"
#include "file_settings.h"
#include "file_process.h"
#include "logic/data_process.h"
#include "time/time_access.h"
#include "time/time_process.h"

uint16_t initialSaveMethod(IN OUT fileObject *object, IN saveFileMethod type)
{
    setSaveMethod(object, type);

    switch (type)
    {
    case FOLDER_METHOD:
        createFolder(object->folderName);

        break;
    case FILE_METHOD:

        break;
    case NO_SAVE_METHOD:
    default:
        if (getSaveMethod(object) == FOLDER_METHOD)
            deleteFolder(object->folderName);

        if (getSaveMethod(object) == FILE_METHOD)
            deleteSpecifyFile("./", object->file.name);
        break;
    }
    return EXECUTE_OK;
}

uint16_t initialSavePath(IN OUT fileObject *object)
{
    uint8_t path[PATH_MAX_LENGTH];
    memset(path, '\0', PATH_MAX_LENGTH);

    switch (object->saveMethod)
    {
    case FOLDER_METHOD:
        sprintf(path, "%s", object->folderName);
        setPath(object, path);

        break;
    case FILE_METHOD:
        sprintf(path, ".");
        setPath(object, path);

        break;
    case NO_SAVE_METHOD:
    default:
        setPath(object, "\0");

        break;
    }
    return EXECUTE_OK;
}

void getFileFullName(IN fileObject *object, OUT char *name)
{
    char fullName[NAME_MAX_LENGTH] = {'\0'};

    sprintf(fullName, "%s", getFileSurname(object));
    if (isFileDateEnable(object))
    {
        sprintf(name, "%s%s", fullName, getFileDate(object));
        strncpy(fullName, name, NAME_MAX_LENGTH);
    }
    if (isFileSerialNumberEnable(object))
    {
        sprintf(name, "%s%d", fullName, (int32_t)getFileSerialNumber(object));
        strncpy(fullName, name, NAME_MAX_LENGTH);
    }
    sprintf(name, "%s.%s", fullName, getFileExtension(object));
}

uint16_t getFilePath(IN fileObject *object, OUT char *filePath)
{
    char path[PATH_MAX_LENGTH] = {'\0'};
    char fullName[NAME_MAX_LENGTH] = {'\0'};

    if (getSaveMethod(object) == NO_SAVE_METHOD)
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    initialSavePath(object);

    sprintf(path, "%s", getPath(object));
    getFileFullName(object, fullName);

    sprintf(filePath, "%s/%s", path, fullName);

    return EXECUTE_OK;
}

uint16_t getBackupPath(IN fileObject *object, OUT char *backupPath)
{
    if (getSaveMethod(object) == NO_SAVE_METHOD)
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    initialSavePath(object);

    sprintf(backupPath, "%s/%s.%s", getPath(object), getFileSurname(object), getBackupExtension(object));

    return EXECUTE_OK;
}

uint16_t deleteFilePastDay(IN fileObject *object)
{
    char dateBuffer[9] = {'\0'};

    if (isExistDayEnable(object))
    {
        getCurrentDate(dateBuffer, 8);
        offsetDate(dateBuffer, -getExistDay(object));

        deleteSpecifyDayAgo(getFolderName(object), getFileSurname(object), dateBuffer + 2);
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

uint16_t deleteOldestFile(IN fileObject *object)
{
    char deleteDate[DATE_MAX_LENGTH] = {'\0'};

    if (isFileDateEnable(object))
    {
        findOldestDate(getFolderName(object), getFileSurname(object), deleteDate);

        deleteSpecifyFile(getFolderName(object), deleteDate);
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

uint16_t checkOneFileSize(IN fileObject *object, IN uint64_t needSize)
{
    uint64_t size;
    char fullName[NAME_MAX_LENGTH] = {'\0'};

    getFileFullName(object, fullName);
    size = getSpecifyFileSize(getFolderName(object), fullName);
    if ((size + needSize) > getOneFileSaveSpace(object))
        return ERR_MISC_NOT_ENOUGH_SPACE;

    return EXECUTE_OK;
}

uint16_t checkAllFileSize(IN fileObject *object)
{
    uint64_t size;

    size = getAllFileSize(getFolderName(object));
    if ((size + getOneFileSaveSpace(object)) > getAllFileSaveSpace(object))
        return ERR_MISC_NOT_ENOUGH_SPACE;

    return EXECUTE_OK;
}

uint16_t writeBackupToTempData(IN fileObject *object, IN const char *format, ...)
{
    uint16_t result = EXECUTE_OK;
    char data[DATA_MAX_LENGTH] = {'\0'};

    va_list ap;
    va_start(ap, format);

    result = vsprintf(data, format, ap);

    va_end(ap);

    if (result > 0)
        setBackupTempData(object, data);

    return result;
}

uint16_t updateBackupFileFromTempData(IN fileObject *object)
{
    char backupPath[PATH_MAX_LENGTH] = {'\0'};
    uint16_t result = 0;

    if (isBackupEnable(object))
    {
        getBackupPath(object, backupPath);

        result = writeFile(backupPath, FILE_MODE_WU, 0, SEEK_SET, getBackupTempData(object), strlen(getBackupTempData(object)));
        if (result != EXECUTE_OK)
            return result;
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

uint16_t restoreDataFromBackup(IN fileObject *object, IN const char *key, OUT char *value)
{
    uint16_t isBackupExist = ERR_MISC_FILE_OPEN_ERROR;
    char backupPath[PATH_MAX_LENGTH] = {'\0'};
    char backupName[NAME_MAX_LENGTH] = {'\0'};
    uint8_t data[100] = {'\0'};
    uint32_t dataLength = sizeof(data);
    uint32_t valueLength = 0;

    if (isBackupEnable(object))
    {
        sprintf(backupName, "%s.%s", getFileSurname(object), getBackupExtension(object));
        if (checkFileExist(getFolderName(object), backupName, backupPath) != EXECUTE_OK)
            return ERR_MISC_FILE_OPEN_ERROR;

        readFile(backupPath, FILE_MODE_R, 0, SEEK_SET, data, &dataLength);

        dictionary(data, key, value, &valueLength);
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

uint16_t initialFileDate(OUT fileObject *object)
{
    char date[DATE_MAX_LENGTH];

    memset(date, '\0', DATE_MAX_LENGTH);
    getFileFormatDate(date);
    setFileDate(object, date);
    return EXECUTE_OK;
}

uint16_t initialFileSerialNumber(IN OUT fileObject *object)
{
    char currentDate[DATE_MAX_LENGTH];
    char backupDate[DATE_MAX_LENGTH];
    uint16_t isBackupExist = ERR_MISC_FILE_OPEN_ERROR;
    char data[10];
    uint32_t value = 0;

    memset(currentDate, '\0', DATE_MAX_LENGTH);
    memset(backupDate, '\0', DATE_MAX_LENGTH);
    memset(data, '\0', 10);

    getFileFormatDate(currentDate);
    isBackupExist = restoreDataFromBackup(object, FILE_KEY_NAME_DATE, backupDate);

    if (isBackupExist == EXECUTE_OK)
    {
        if (strncmp(currentDate, backupDate, DATE_MAX_LENGTH) == 0)
        {
            restoreDataFromBackup(object, FILE_KEY_NAME_SN, data);
            value = atoi(data);
            setFileSerialNumber(object, value);
        }
        else
        {
            setFileSerialNumber(object, 0);
        }
    }
    else
    {
        value = getFileSerialNumberFromFolder(object->folderName, currentDate);
        setFileSerialNumber(object, value);
    }
    return EXECUTE_OK;
}

uint16_t rollSerialNumber(IN OUT fileObject *object)
{
    uint32_t serialNumber = 0;
    if (isFileSerialNumberEnable(object))
    {
        serialNumber = getFileSerialNumber(object) + 1;
        if (serialNumber > 9)
        {
            serialNumber = 0;
        }
        setFileSerialNumber(object, serialNumber);
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

uint16_t checkDate(IN OUT fileObject *object)
{
    uint8_t fileDate[DATE_MAX_LENGTH] = {'\0'};
    uint8_t currentDate[DATE_MAX_LENGTH] = {'\0'};

    if (isFileDateEnable(object))
    {
        strncpy(fileDate, getFileDate(object), DATE_MAX_LENGTH);
        getFileFormatDate(currentDate);

        if (strcmp(fileDate, currentDate) < 0)
            setFileDate(object, currentDate);
    }
    else
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    return EXECUTE_OK;
}

void createLog(IN OUT fileObject *object)
{
    initialFileObject(object);
    enableFileDate(object);
    enableFileSerialNumber(object);
    enableBackup(object);
    enableAllFileSaveSpace(object);
    enableOneFileSaveSpace(object);
    enableExistDay(object);
    folderRename(object, "Log");
    fileRename(object, "IPLOG");
    fileExtensionRename(object, "dat");
    setAllFileMaxSpace(object, 10000000);
    setOneFileMaxSpace(object, 100000);
    setFileExistDay(object, 30);
    initialSaveMethod(object, FOLDER_METHOD);
    initialSavePath(object);
    initialFileDate(object);
    initialFileSerialNumber(object);
    deleteFilePastDay(object);
}

uint16_t writeProcess(IN OUT fileObject *object, IN OUT char *mode, IN int seek, IN uint8_t *data, IN uint32_t datalen)
{
    char path[PATH_MAX_LENGTH] = {'\0'};

    if (getSaveMethod(object) == NO_SAVE_METHOD)
        return ERR_MISC_NOT_ENABLE_FUNCTION;

    checkDate(object);
    if (checkAllFileSize(object) == ERR_MISC_NOT_ENOUGH_SPACE)
        deleteOldestFile(object);
    if (checkOneFileSize(object, datalen) == ERR_MISC_NOT_ENOUGH_SPACE)
    {
        rollSerialNumber(object);
        mode = FILE_MODE_WU;
    }

    getFilePath(object, path);

    writeFile(path, mode, 0, seek, data, datalen);

    updateBackupFileFromTempData(object);

    return EXECUTE_OK;
}

char filterChar(char aChar)
{
    uint32_t i = 0;

    // 0 - 9
    for (i = 48; i < 58; ++i)
    {
        if (aChar == i)
            return (char)i;
    }
    // 'A' - 'Z'
    for (i = 65; i < 91; ++i)
    {
        if (aChar == i)
            return (char)i;
    }
    // 'a' - 'z'
    for (i = 97; i < 123; ++i)
    {
        if (aChar == i)
            return (char)i;
    }
    // other valid characters
    switch (aChar)
    {
    case '/':
        return '/';
    case '.':
        return '.';
    case '-':
        return '-';
    case '_':
        return '_';
    case ' ':
        return ' ';
    case '\\':
        return '\\';
    case ':':
        return ':';
    }
    return '%';
}

const char *filterPath(char *path)
{
    uint32_t i = 0;
    static char filteredPath[PATH_MAX_LENGTH];
    memset(filteredPath, '\0', PATH_MAX_LENGTH);

    for (i = 0; i < strlen(path); i++)
    {
        filteredPath[i] = filterChar(path[i]);
    }

    return filteredPath;
}
