#include "file_settings.h"
#include "file_access.h"
#include "time/time_access.h"
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#elif __linux
#include <unistd.h>
#include <sys/types.h>
#endif

void initialFileObject(IN OUT fileObject *object)
{
    memset(object, '\0', sizeof(fileObject));

    memset((char *)object->path, '\0', PATH_MAX_LENGTH);

    sprintf((char *)object->folderName, FOLDER_NAME);

    sprintf((char *)object->file.name, FILE_NAME);
    sprintf((char *)object->file.extension, FILE_EXTENSION);
    object->file.date.enable = 0;
    sprintf((char *)object->file.date.ADYear, "000000");
    object->file.serialNumber.enable = 0;
    object->file.serialNumber.value = 0;

    object->backup.enable = 0;
    sprintf((char *)object->backup.extension, BACKUP_EXTENSION);
    memset(object->backup.tempData, '\0', DATA_MAX_LENGTH);

    object->saveMethod = NO_SAVE_METHOD;

    object->saveSpace.allFileMax.enable = 0;
    object->saveSpace.allFileMax.value = ALL_FILE_MAX_SPACE;
    object->saveSpace.oneFileMax.enable = 0;
    object->saveSpace.oneFileMax.value = ONE_FILE_MAX_SPACE;
    object->saveSpace.existDay.enable = 0;
    object->saveSpace.existDay.value = FILE_EXIST_DAY;
}

const char *getPath(IN fileObject *object)
{
    return object->path;
}

void setPath(IN fileObject *object, IN const char *path)
{
    memset(object->path, '\0', PATH_MAX_LENGTH);
    sprintf(object->path, "%s", path);
}

const char *getFolderName(IN fileObject *object)
{
    return object->folderName;
}

void folderRename(IN OUT fileObject *object, IN const char *rename)
{
    memset(object->folderName, '\0', NAME_MAX_LENGTH);
    sprintf(object->folderName, "%s", rename);
}

const char *getFileSurname(IN fileObject *object)
{
    return object->file.name;
}

void fileRename(IN OUT fileObject *object, IN const char *rename)
{
    memset(object->file.name, '\0', NAME_MAX_LENGTH);
    sprintf(object->file.name, "%s", rename);
}

const char *getFileExtension(IN fileObject *object)
{
    return object->file.extension;
}

void fileExtensionRename(IN OUT fileObject *object, IN const char *rename)
{
    memset(object->file.extension, '\0', EXTENSION_MAX_LENGTH);
    sprintf(object->file.extension, "%s", rename);
}

uint8_t isFileDateEnable(IN fileObject *object)
{
    return object->file.date.enable;
}

void enableFileDate(IN fileObject *object)
{
    object->file.date.enable = 1;
}

void disableFileDate(IN fileObject *object)
{
    object->file.date.enable = 0;
}

const char *getFileDate(IN fileObject *object)
{
    return object->file.date.ADYear;
}

void setFileDate(IN fileObject *object, IN const char *date)
{
    memcpy(object->file.date.ADYear, date, DATE_MAX_LENGTH);
}

uint8_t isFileSerialNumberEnable(IN fileObject *object)
{
    return object->file.serialNumber.enable;
}

void enableFileSerialNumber(IN fileObject *object)
{
    object->file.serialNumber.enable = 1;
    enableOneFileSaveSpace(object);
}

void disableFileSerialNumber(IN fileObject *object)
{
    object->file.serialNumber.enable = 0;
}

uint32_t getFileSerialNumber(IN fileObject *object)
{
    return object->file.serialNumber.value;
}

void setFileSerialNumber(IN fileObject *object, IN uint32_t value)
{
    object->file.serialNumber.value = value;
}

uint8_t isBackupEnable(IN fileObject *object)
{
    return object->backup.enable;
}

void enableBackup(IN fileObject *object)
{
    object->backup.enable = 1;
}

void disableBackup(IN fileObject *object)
{
    object->backup.enable = 0;
}

const char *getBackupExtension(IN fileObject *object)
{
    return object->backup.extension;
}

void backupExtensionRename(IN OUT fileObject *object, IN const char *rename)
{
    memset(object->backup.extension, '\0', EXTENSION_MAX_LENGTH);
    sprintf(object->backup.extension, "%s", rename);
}

char *getBackupTempData(IN fileObject *object)
{
    return object->backup.tempData;
}

void setBackupTempData(IN OUT fileObject *object, IN const char *tempData)
{
    memset(object->backup.tempData, '\0', DATA_MAX_LENGTH);
    sprintf(object->backup.tempData, "%s", tempData);
}

saveFileMethod getSaveMethod(IN fileObject *object)
{
    return object->saveMethod;
}

void setSaveMethod(IN fileObject *object, IN saveFileMethod method)
{
    object->saveMethod = method;
}

uint8_t isAllFileSaveSpaceEnable(IN fileObject *object)
{
    return object->saveSpace.allFileMax.enable;
}

uint8_t enableAllFileSaveSpace(IN fileObject *object)
{
    object->saveSpace.allFileMax.enable = 1;
    enableFileDate(object);
}

uint8_t disableAllFileSaveSpace(IN fileObject *object)
{
    object->saveSpace.allFileMax.enable = 0;
}

uint64_t getAllFileSaveSpace(IN fileObject *object)
{
    return object->saveSpace.allFileMax.value;
}

void setAllFileMaxSpace(IN OUT fileObject *object, IN uint64_t maxSpace)
{
    object->saveSpace.allFileMax.value = maxSpace;
}

uint8_t isOneFileSaveSpaceEnable(IN fileObject *object)
{
    return object->saveSpace.oneFileMax.enable;
}

uint8_t enableOneFileSaveSpace(IN fileObject *object)
{
    object->saveSpace.oneFileMax.enable = 1;
}

uint8_t disableOneFileSaveSpace(IN fileObject *object)
{
    object->saveSpace.oneFileMax.enable = 0;
}

uint64_t getOneFileSaveSpace(IN fileObject *object)
{
    return object->saveSpace.oneFileMax.value;
}

void setOneFileMaxSpace(IN OUT fileObject *object, IN uint64_t maxSpace)
{
    object->saveSpace.oneFileMax.value = maxSpace;
}

uint8_t isExistDayEnable(IN fileObject *object)
{
    return object->saveSpace.existDay.enable;
}

uint8_t enableExistDay(IN fileObject *object)
{
    object->saveSpace.existDay.enable = 1;
}

uint8_t disableExistDay(IN fileObject *object)
{
    object->saveSpace.existDay.enable = 0;
}

uint64_t getExistDay(IN fileObject *object)
{
    return object->saveSpace.existDay.value;
}

void setFileExistDay(IN OUT fileObject *object, IN uint64_t existDay)
{
    object->saveSpace.existDay.value = existDay;
}

void getFileFormatDate(char *date)
{
    char dateBuffer[10];
    getCurrentDate(dateBuffer, sizeof(dateBuffer));
    memcpy(date, dateBuffer + 2, 6);
}
