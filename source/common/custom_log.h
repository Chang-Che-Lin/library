#ifndef _CUSTOM_LOG_H
#define _CUSTOM_LOG_H

#include <stdio.h>

#include "custom_types.h"
#include "custom_errors.h"

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void enableLog(void);
void setLogName(IN const char *name);
void setLogPath(IN const char *path);
void setAllLogMaxSpace(IN uint32_t maxSpace);
void setOneLogMaxSpace(IN uint32_t maxSpace);
void setLogExistDay(IN uint32_t existDay);
void setLogSaveMethod(IN uint32_t type);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // _CUSTOM_LOG_H