#include <string.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "common/custom_errors.h"
#include "file/file_settings.h"
#include "file/file_process.h"
#include "file/file_access.h"
#include "mock/time_mock.hpp"

using Catch::Matchers::Equals;

SCENARIO("initial file module")
{
    GIVEN("create a file module")
    {
        std::string stringResult;
        uint32_t valueResult = 0xFFFF;
        fileObject log;

        WHEN("initial the module")
        {
            initialFileObject(&log);

            THEN("folder name is FOLDER_NAME")
            {
                stringResult = getFolderName(&log);
                CHECK_THAT(stringResult, Equals(FOLDER_NAME));
            }

            THEN("file name is FILE_NAME")
            {
                stringResult = getFileSurname(&log);
                CHECK_THAT(stringResult, Equals(FILE_NAME));
            }

            THEN("file extension is FILE_EXTENSION")
            {
                stringResult = getFileExtension(&log);
                CHECK_THAT(stringResult, Equals(FILE_EXTENSION));
            }

            THEN("file date enable is 0")
            {
                valueResult = isFileDateEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("file date string is '000000'")
            {
                stringResult = getFileDate(&log);
                CHECK_THAT(stringResult, Equals("000000"));
            }

            THEN("file serialNumber enable is 0")
            {
                valueResult = isFileSerialNumberEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("file serialNumber value is 0")
            {
                valueResult = getFileSerialNumber(&log);
                CHECK(valueResult == 0);
            }

            THEN("backup enable is 0")
            {
                valueResult = isBackupEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("backup extension is BACKUP_EXTENSION")
            {
                stringResult = getBackupExtension(&log);
                CHECK_THAT(stringResult, Equals(BACKUP_EXTENSION));
            }

            THEN("save method is NO_SAVE_METHOD")
            {
                valueResult = getSaveMethod(&log);
                CHECK(valueResult == NO_SAVE_METHOD);
            }

            THEN("all file max enable is 0")
            {
                valueResult = isAllFileSaveSpaceEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("all file max value is ALL_FILE_MAX_SPACE")
            {
                valueResult = getAllFileSaveSpace(&log);
                CHECK(valueResult == ALL_FILE_MAX_SPACE);
            }

            THEN("one file max enable is 0")
            {
                valueResult = isOneFileSaveSpaceEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("one file max value is ONE_FILE_MAX_SPACE")
            {
                valueResult = getOneFileSaveSpace(&log);
                CHECK(valueResult == ONE_FILE_MAX_SPACE);
            }

            THEN("exist day enable is 0")
            {
                valueResult = isExistDayEnable(&log);
                CHECK(valueResult == 0);
            }

            THEN("exist day enable is FILE_EXIST_DAY")
            {
                valueResult = getExistDay(&log);
                CHECK(valueResult == FILE_EXIST_DAY);
            }
        }
    }
}

SCENARIO("set path")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set path is 'test/log.txt'")
        {
            setPath(&log, "test/log.txt");

            THEN("result is 'test/log.txt'")
            {
                result = getPath(&log);
                CHECK_THAT(result, Equals("test/log.txt"));
            }
        }

        WHEN("initial path to '\0'")
        {
            setPath(&log, "\0");

            THEN("result is '\0'")
            {
                result = getPath(&log);
                CHECK_THAT(result, Equals(""));
            }
        }
    }
}

SCENARIO("rename folder name")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("rename folder name to 'test'")
        {
            folderRename(&log, "test");

            THEN("result is 'test'")
            {
                result = getFolderName(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("rename file name")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("rename file name to 'test'")
        {
            fileRename(&log, "test");

            THEN("result is 'test'")
            {
                result = getFileSurname(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("rename file extension")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("rename file extension to 'test'")
        {
            fileExtensionRename(&log, "test");

            THEN("result is 'test'")
            {
                result = getFileExtension(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("rename backup extension")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("rename backup extension to 'test'")
        {
            backupExtensionRename(&log, "test");

            THEN("result is 'test'")
            {
                result = getBackupExtension(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("set backup data")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set backup data is 'test'")
        {
            setBackupTempData(&log, "test");

            THEN("result is 'test'")
            {
                result = getBackupTempData(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("determine whether to enable file date")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable file date")
        {
            enableFileDate(&log);

            THEN("result is 1")
            {
                result = isFileDateEnable(&log);
                CHECK(result == 1);
            }

            disableFileDate(&log);

            THEN("result is 0")
            {
                result = isFileDateEnable(&log);
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("set file date")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set file date is '210805'")
        {
            setFileDate(&log, "210805");

            THEN("result is '210805'")
            {
                result = getFileDate(&log);
                CHECK_THAT(result, Equals("210805"));
            }
        }
    }
}

SCENARIO("determine whether to enable serial number")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable file date")
        {
            enableFileSerialNumber(&log);

            THEN("result is 1")
            {
                result = isFileSerialNumberEnable(&log);
                CHECK(result == 1);
            }

            THEN("one file save also enable")
            {
                result = isOneFileSaveSpaceEnable(&log);
                CHECK(result == 1);
            }

            disableFileSerialNumber(&log);

            THEN("result is 0")
            {
                result = isFileSerialNumberEnable(&log);
                CHECK(result == 0);
            }

            THEN("one file save still enable")
            {
                result = isOneFileSaveSpaceEnable(&log);
                CHECK(result == 1);
            }
        }
    }
}

SCENARIO("set serial number")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set serial number is 6")
        {
            setFileSerialNumber(&log, 6);

            THEN("result is 6")
            {
                result = getFileSerialNumber(&log);
                CHECK(result == 6);
            }
        }
    }
}

SCENARIO("determine whether to enable backup")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable backup")
        {
            enableBackup(&log);

            THEN("result is 1")
            {
                result = isBackupEnable(&log);
                CHECK(result == 1);
            }

            disableBackup(&log);

            THEN("result is 0")
            {
                result = isBackupEnable(&log);
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("backup extension rename")
{
    GIVEN("create a file module")
    {
        std::string result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set backup extension is 'test'")
        {
            backupExtensionRename(&log, "test");

            THEN("result is 'test'")
            {
                result = getBackupExtension(&log);
                CHECK_THAT(result, Equals("test"));
            }
        }
    }
}

SCENARIO("set save method")
{
    GIVEN("create a file module")
    {
        saveFileMethod result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set save method")
        {
            setSaveMethod(&log, FILE_METHOD);

            THEN("save method is FILE_METHOD")
            {
                result = getSaveMethod(&log);
                CHECK(result == FILE_METHOD);
            }
        }
    }
}

SCENARIO("determine whether to enable all file save space")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable all file save space")
        {
            enableAllFileSaveSpace(&log);

            THEN("result is 1")
            {
                result = isAllFileSaveSpaceEnable(&log);
                CHECK(result == 1);
            }

            THEN("file date also enable")
            {
                result = isFileDateEnable(&log);
                CHECK(result == 1);
            }

            disableAllFileSaveSpace(&log);

            THEN("result is 0")
            {
                result = isAllFileSaveSpaceEnable(&log);
                CHECK(result == 0);
            }

            THEN("file date still enable")
            {
                result = isFileDateEnable(&log);
                CHECK(result == 1);
            }
        }
    }
}

SCENARIO("determine whether to enable one file save space")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable one file save space")
        {
            enableOneFileSaveSpace(&log);

            THEN("result is 1")
            {
                result = isOneFileSaveSpaceEnable(&log);
                CHECK(result == 1);
            }

            disableOneFileSaveSpace(&log);

            THEN("result is 0")
            {
                result = isOneFileSaveSpaceEnable(&log);
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("determine whether to enable exist day")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("enable or disable exist day")
        {
            enableExistDay(&log);

            THEN("result is 1")
            {
                result = isExistDayEnable(&log);
                CHECK(result == 1);
            }

            disableExistDay(&log);

            THEN("result is 0")
            {
                result = isExistDayEnable(&log);
                CHECK(result == 0);
            }
        }
    }
}

SCENARIO("set save file space")
{
    GIVEN("create a file module")
    {
        uint32_t result;
        fileObject log;

        initialFileObject(&log);

        WHEN("set all file max space")
        {
            setAllFileMaxSpace(&log, 500);
            result = getAllFileSaveSpace(&log);

            THEN("all file max space is 500")
            {
                CHECK(result == 500);
            }
        }

        WHEN("set one file max space")
        {
            setOneFileMaxSpace(&log, 50);
            result = getOneFileSaveSpace(&log);

            THEN("one file max space is 50")
            {
                CHECK(result == 50);
            }
        }

        WHEN("set file exist day")
        {
            setFileExistDay(&log, 1);
            result = getExistDay(&log);

            THEN("file exist day is 1")
            {
                CHECK(result == 1);
            }
        }
        setAllFileMaxSpace(&log, ALL_FILE_MAX_SPACE);
        setOneFileMaxSpace(&log, ONE_FILE_MAX_SPACE);
        setFileExistDay(&log, FILE_EXIST_DAY);
    }
}

SCENARIO("get file format date")
{
    GIVEN("a date buffer")
    {
        std::string result;
        char dateBuffer[10] = {'\0'};
        WHEN("get date")
        {
            ALLOW_CALL(stubDate, getCurrentDate(_, _)).LR_SIDE_EFFECT(memcpy(_1, "20210716", 9));
            getFileFormatDate(dateBuffer);

            THEN("result is ERR_MISC_FILE_OPEN_ERROR")
            {
                result = dateBuffer;
                CHECK_THAT(result, Equals("210716"));
            }
        }
    }
}
