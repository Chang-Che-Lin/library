#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "encrypt/encrypt.h"

using Catch::Matchers::Equals;

SCENARIO("calculate crc8")
{
    GIVEN("create a character is 0xE8")
    {
        uint8_t head = 0xE8;

        WHEN("calculate crc8")
        {
            int32_t result = 0xFFFF;
            result = crc8(&head, 1);

            THEN("result is 0x49")
            {
                CHECK(result == 0x49);
            }
        }
    }
}
