#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_factory.h"
#include "message/message_check.h"
#include "message/message_head.h"

using Catch::Matchers::Equals;

SCENARIO("process check")
{
    GIVEN("create a component object and binding head")
    {
        int32_t result = false;
        messageComponents *Components = makeComponents(1);
        uint8_t buffer[] = {1, 2, 3, 4};

        Components->binding[0] = bindingCRC("HEAD", "test1 test2 aaa3", 3);

        WHEN("process correct head")
        {
            uint8_t data[1];
            data[0] = 0xE8;

            result = Components->binding[0]->process(Components, 0, data, sizeof(data) / sizeof(data[0]));

            // THEN("result is true")
            // {
            //     CHECK(result == false);
            // }
        }

        // WHEN("process incorrect head")
        // {
        //     uint8_t data[1];
        //     data[0] = 0xE9;

        //     result = Components->binding[0]->process(Components, 0, data, sizeof(data) / sizeof(data[0]));

        //     THEN("result is false")
        //     {
        //         CHECK(result == false);
        //     }
        // }

        freeComponent(Components);
    }
}
