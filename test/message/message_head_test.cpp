#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_factory.h"
#include "message/message_head.h"

using Catch::Matchers::Equals;

SCENARIO("process head")
{
    GIVEN("create a component object and binding head")
    {
        int32_t result = false;
        messageComponents *Components = makeComponents(1);

        Components->binding[0] = bindingHead("HEAD", 0xE8);

        WHEN("process correct head")
        {
            uint8_t data[1];
            data[0] = 0xE8;

            result = Components->binding[0]->process(Components, 0, data, sizeof(data) / sizeof(data[0]));

            THEN("result is true")
            {
                CHECK(result == true);
            }
        }

        WHEN("process incorrect head")
        {
            uint8_t data[1];
            data[0] = 0xE9;

            result = Components->binding[0]->process(Components, 0, data, sizeof(data) / sizeof(data[0]));

            THEN("result is false")
            {
                CHECK(result == false);
            }
        }

        freeComponent(Components);
    }
}
