#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message/message_cJSON.h"

using Catch::Matchers::Equals;

SCENARIO("cJSON get data")
{
    GIVEN("create a cJSON object")
    {
        cJSON *object = cJSON_CreateObject();

        cJSON_AddStringToObject(object, "str", "test");
        cJSON_AddNumberToObject(object, "num", 20);

        WHEN("get object number")
        {
            int32_t result = 0xFFFF;
            result = cJSON_getItemNumber(object, "num");

            THEN("number is 20")
            {
                CHECK(result == 20);
            }
        }

        WHEN("get object string")
        {
            std::string result;
            result = cJSON_getItemString(object, "str");

            THEN("string is 'test'")
            {
                CHECK_THAT(result, Equals("test"));
            }
        }
        cJSON_Delete(object);
    }
}

SCENARIO("cJSON reset data")
{
    GIVEN("create a cJSON object")
    {
        cJSON *object = cJSON_CreateObject();

        cJSON_AddStringToObject(object, "str", "test");
        cJSON_AddNumberToObject(object, "num", 20);

        WHEN("get object number")
        {
            int32_t result = 0xFFFF;
            cJSON_resetItemNumber(object, "num", 15);
            result = cJSON_getItemNumber(object, "num");

            THEN("number change to 15 from 20")
            {
                CHECK(result == 15);
            }
        }

        WHEN("get object string")
        {
            std::string result;
            cJSON_resetItemString(object, "str", "abcd");
            result = cJSON_getItemString(object, "str");

            THEN("string change to 'abcd' from 'test'")
            {
                CHECK_THAT(result, Equals("abcd"));
            }
        }
        cJSON_Delete(object);
    }
}
