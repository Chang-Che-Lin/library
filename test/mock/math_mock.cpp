#include "math_mock.hpp"

STUB_MATH stubMath;

extern "C" {
int numberDouble(int num)
{
    return stubMath.numberDouble(num);
}
}