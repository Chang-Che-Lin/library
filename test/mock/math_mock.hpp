#ifndef _MATH_MOCK_H
#define _MATH_MOCK_H

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>

#include "common/custom_math.h"
using trompeloeil::_;

class STUB_MATH
{
  public:
    MAKE_MOCK1(numberDouble, int(int));
};

extern STUB_MATH stubMath;

#endif // _MATH_MOCK_H