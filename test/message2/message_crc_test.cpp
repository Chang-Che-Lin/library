#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <catch2/catch_all.hpp>
#include <catch2/trompeloeil.hpp>
#include <catch2/matchers/catch_matchers_string.hpp>

#include "message2/message.h"
#include "message2/message_component.h"

using Catch::Matchers::Equals;

#ifdef __cplusplus
extern "C" {
#endif
  void message_CRC8Make(void *specifyModule, void *specifyComponent);
#ifdef __cplusplus
}
#endif

SCENARIO("crc")
{
  void *otaMessage = message_moduleNew("ota");

  GIVEN("register variable component")
  {
    message_bindingVariableCommand(otaMessage, "Cmd", 1);
    message_bindingVariableData(otaMessage, "Data", 1);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    WHEN("receive data include correct CRC")
    {
      message_receiveHandler(otaMessage, "\x02\x31\x32\x02\x33\x34\x01", 7);

      THEN("CRC correct and state be set done")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateDone);
      }
    }

    WHEN("receive data include incorrect CRC")
    {
      message_receiveHandler(otaMessage, "\x02\x31\x32\x02\x33\x34\x02", 7);

      THEN("CRC incorrect and state be set undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateUndo);
      }
    }

    WHEN("set Cmd & Data send data")
    {
      message_sendSaveData(otaMessage, "Cmd", "123", 3);
      message_sendSaveData(otaMessage, "Data", "456", 3);

      THEN("make crc8 from sendData, and crc8 is 0xC8")
      {
        std::string result;
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        message_CRC8Make(otaMessage, specifyComponent);

        result = (char *)specifyComponent->sendData;

        CHECK_THAT(result, Equals("\xC8"));
      }
    }
  }

  GIVEN("register static component")
  {
    message_bindingStaticCommand(otaMessage, "Cmd", 2);
    message_bindingStaticData(otaMessage, "Data", 2);
    message_bindingCRC8(otaMessage, "CRC", "Cmd");

    WHEN("receive data include correct CRC")
    {
      message_receiveHandler(otaMessage, "\x31\x32\x33\x34\x16", 5);

      THEN("CRC correct and state be set done")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateDone);
      }
    }

    WHEN("receive data include incorrect CRC")
    {
      message_receiveHandler(otaMessage, "\x31\x32\x33\x34\x17", 5);

      THEN("CRC incorrect and state be set undo")
      {
        messageComponent *specifyComponent = message_componentGetSpecify(otaMessage, "CRC");
        uint32_t result = specifyComponent->state;
        CHECK(result == stateUndo);
      }
    }
  }

  message_moduleFree(otaMessage);
}
